FROM ruby:2.3

COPY	./Gemfile /
RUN	gem install bundler && \
	bundle install && \
	rm -f /Gemfile
