---
layout: default
---

# SECUQAM

SECUQAM est un regroupement de passionné.es par tous les aspects touchant
à la cybersécurité, et intéressé.e à apprendre, partager et collaborer.
Composé d'étudiant.es de tous les cycles et d'enseignant.es de l'UQAM, il
vise à promouvoir, et supporter le développement des connaissances et
compétences en sécurité informatique. Les évènements organisés sont
ouvert à tous et sans discrimination.

<!-- 
Présentation du dernier post
-->
{% for post in site.posts limit:1 %}
# {{ post.title }}
{{ post.content }}
{% endfor %}

# ÉVÉNEMENTS PASSÉS:

<!-- 
Présentation des deux posts avant-derniers posts
-->
{% for post in site.posts offset:1 limit:2 %}
{{ post.content }}
{% endfor %}


# Rencontres de l'automne 2019

Noter que, afin de répondre à la disponibilité du plus grand nombre de
participant.es, l'horaire des rencontres est sujet à changer selon la
structure des sessions de cours.

* Quand? Les premiers et troisièmes jeudis du mois à 12:00
* Où? Au local PK-4610
* Qui? C'est ouvert à tous
* Quoi? Présentations, ateliers, rencontres et discussions


# Crédits

* Ce site est basé sur le thème [jekyll-console](https://jekyll-themes.com/jekyll-console/)
