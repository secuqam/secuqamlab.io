---
title: "Atelier d'introduction aux compétitions de sécurité (CTFs)"
---

# Deuxième édition : 3 octobre 2019
BIENVENUE AUX DÉBUTANT.E.S !

Pour la deuxième édition de SECUQAM nous présenterons une
introduction aux compétitions de sécurité informatique aussi
appelées Catch the Flag ou CTF. Cela permettra aux participant.es
de découvrir ou de se dérouiller les doigts sur une série d'exercices
pour tous les niveaux, tels que vous en verrez dans les compétitions.
APPORTEZ VOTRE ORDINATEUR PORTABLE/LAPTOP !

* Quand? 3 octobre 2019
  * Manger ensemble à 12h (midi), apportez votre lunch
  * Début de l'atelier à 12h30 (midi et demi)
* Où? Local PK-4610
* Qui? C'est ouvert à tou.te.s
* [Inscrivez-vous](http://xymus.net/opportunity/?meetup_id=CB0BFB4A268174A1025B3E9FE1A11503DFD07B3F)
