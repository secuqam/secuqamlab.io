---
title: "Retour sur le NorthSec"
---

# Première édition: 5 juin 2019

Pour la première édition de SECUQAM nous présenterons le
[NorthSec](https://nsec.io/), les types d'épreuves, les
solutions originales des participant.es et l'ambiance spéciale de
l'évènement. Cela permettra aux participant.es de partager leurs
impressions et de faire découvrir l'expérience de ce CTF unique à
celles et ceux qui s'y intéressent.

* Quand? 5 juin 2019 à 18h
* Où? Local PK-4610
* Qui? C'est ouvert à tou.te.s
* [Inscrivez-vous](http://xymus.net/opportunity/?meetup_id=155FF57BEA2D0AD9FFE57865E07BCA79D0B95497)
