---
title: "Atelier CTF Top 10 OWASP du Hackfest"
---

# Troisième édition: 21 novembre 2019

Cet atelier portera sur des challenges sur le top 10 de l'OWASP présentés au
[Hackfest](https://hackfest.ca/), vous pourrez jouer, verrez les
solutions originales des participant.es et l'ambiance spéciale de
l'évènement. Cela permettra aux participant.es de partager leurs
impressions et de faire découvrir des challenges classiques du web à
celles et ceux qui s'y intéressent !

* Quand? 21 novembre 2019 à 12h (midi)
* Où? Local PK-4610
* Qui? C'est ouvert à tou.te.s
* [Inscrivez-vous](https://xymus.net/opportunity/?meetup_id=1637EA23AAD05E1A7B02C8FB6D228C4071285FE7)
