---
layout: page
title: À propos
permalink: /apropos/
---

SECUQAM est un regroupement de passionné.es par tous les aspects touchant
à la cybersécurité, et intéressé.e à partager, collaborer et apprendre.
Composé d'étudiant.es de tous les cycles et d'enseignant.es de l'UQAM, il
vise à promouvoir, et supporter le développement des connaissances et
compétences en sécurité informatique.

## Membres organisateurs

* Philippe Pépos Petitclerc (président)
* Jean Privat (professeur responsable)
* Alexandre Côté Cyr (@barberousse)
* Philippe Grégoire (@fob)
* Luis-Gaylor Nobre
* Nicolas Lamoureux (@nic-lovin)
