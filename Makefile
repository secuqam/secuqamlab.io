

all: build
	cd src && jekyll build

docker-img:
	docker build -t secuqam.gitlab.io .

docker: docker-img
	docker run --rm -it -u $$(id -u):$$(id -g) -v "$$(pwd):/media" \
		secuqam.gitlab.io \
		/bin/bash -c '\
			ip=$$(ip a s eth0 | grep "inet " | xargs echo | \
				cut -d" " -f2 | cut -d/ -f1); \
			cd /media/src; \
			jekyll serve --incremental -H "$$ip" \
		'
